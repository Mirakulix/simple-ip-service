package de.mirakulix.model;

import lombok.Data;

@Data
public class Currency {
    private String code;
    private String name;
    private Character symbol;
}
