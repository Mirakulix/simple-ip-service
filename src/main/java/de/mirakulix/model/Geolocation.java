package de.mirakulix.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Geolocation {
    private String ip;
    private String hostname;
    @JsonProperty("continent_code")
    private String continentCode;
    @JsonProperty("continent_name")
    private String continentName;
    @JsonProperty("country_code2")
    private String countryCode2;
    @JsonProperty("country_code3")
    private String countryCode3;
    @JsonProperty("country_name")
    private String countryName;
    @JsonProperty("country_capital")
    private String countryCapital;
    @JsonProperty("state_prov")
    private String stateProv;
    private String district;
    private String city;
    private String zipcode;
    private String latitude;
    private String longitude;
    @JsonProperty("is_eu")
    private boolean eu;
    @JsonProperty("calling_code")
    private String callingCode;
    @JsonProperty("country_tld")
    private String countryTld;
    private String languages;
    @JsonProperty("country_flag")
    private String countryFlag;
    private String isp;
    @JsonProperty("connection_type")
    private String connectionType;
    private String organization;
    @JsonProperty("geoname_id")
    private String geonameId;

    private Currency currency;
    private TimeZone timeZone;
}
