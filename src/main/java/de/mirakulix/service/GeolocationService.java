package de.mirakulix.service;

import de.mirakulix.model.Geolocation;
import de.mirakulix.proxy.IpGeolocationProxy;
import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Singleton;

@Slf4j
@Singleton
public class GeolocationService {

    @Inject
    private final IpGeolocationProxy ipGeoProxy;

    public GeolocationService(IpGeolocationProxy ipGeoProxy) {
        this.ipGeoProxy = ipGeoProxy;
    }

    public Geolocation getGeolocationByIp(String ip) {
        try {
            Geolocation geolocationJson = ipGeoProxy.getGeolocation(ip);
            return geolocationJson;
        } catch (Exception e) {
            log.error("error: " + e);
            return null;
        }
    }
}
