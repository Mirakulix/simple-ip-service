package de.mirakulix.controller;

import de.mirakulix.model.Geolocation;
import de.mirakulix.service.GeolocationService;
import io.micronaut.http.HttpHeaders;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

import java.util.List;

@Controller
public class LocationController implements Base {

    private final GeolocationService geolocationService;

    public LocationController(GeolocationService geolocationService) {
        this.geolocationService = geolocationService;
    }

    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404")
    })
    @Get(uri = "/geolocation/{ipv4}", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<Geolocation> getGeoLocation(@PathVariable String ipv4) {
        Geolocation geolocation = geolocationService.getGeolocationByIp(ipv4);
        return simpleResponse(geolocation);
    }

    @ApiResponses({
            @ApiResponse(responseCode = "200"),
            @ApiResponse(responseCode = "404")
    })
    @Get(uri = "/geolocation", produces = MediaType.APPLICATION_JSON)
    public HttpResponse<Geolocation> getDefaultGeoLocation(@Header("x-forwarded-for") String ip) {

        Geolocation geolocation = geolocationService.getGeolocationByIp(ip);
        return simpleResponse(geolocation);
    }
}
