package de.mirakulix.controller;

import io.micronaut.http.HttpResponse;

public interface Base {

    default <T> HttpResponse<T> simpleResponse (T object) {
        return object == null ?
                HttpResponse.notFound() :
                HttpResponse.ok(object);
    }
}
