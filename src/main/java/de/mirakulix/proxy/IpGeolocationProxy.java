package de.mirakulix.proxy;

import com.fasterxml.jackson.annotation.JsonCreator;
import de.mirakulix.model.Geolocation;
import io.micronaut.context.annotation.Property;
import io.micronaut.context.annotation.Value;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;

@Client("${base-url}")
public interface IpGeolocationProxy {

    @Get("/ipgeo?apiKey=${api-key}&lang=de&ip={ip}")
    Geolocation getGeolocation(String ip);
}

