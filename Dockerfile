FROM registry.gitlab.com/mirakulix/alpine:java-hotspot-builder AS builder

RUN mkdir /app
WORKDIR /app

RUN jlink \
    --compress=0 \
    --module-path jmod \
    --add-modules java.base,java.xml,jdk.unsupported,java.sql,java.naming,java.desktop,java.management,java.security.jgss,java.instrument,jdk.management.agent,jdk.zipfs \
    --output /jlinked

COPY build.gradle /app
COPY gradle.properties /app
COPY src /app/src

RUN gradle assemble


FROM registry.gitlab.com/mirakulix/alpine:java-runner

COPY --from=builder /jlinked /opt/jre
COPY --from=builder /app/build/libs/app-0.1-all.jar /app.jar

EXPOSE 80

CMD /opt/jre/bin/java -jar /app.jar